import pytest
import upload
import boto3
from moto import mock_s3

@mock_s3

def test_upload():
    conn = boto3.client('s3', region_name='us-east-1')
    # We need to create the bucket since this is all in Moto's 'virtual' AWS account
    bucket = 'comp630-m1-f19'
    conn.create_bucket(Bucket=bucket)
    location = "test.boto"
    upload.upload_file(location, bucket, "kdl1.boto")
    assert True